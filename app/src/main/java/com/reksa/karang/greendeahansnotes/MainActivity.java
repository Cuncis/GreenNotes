package com.reksa.karang.greendeahansnotes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnTambahData, btnLihatData, btnKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTambahData = findViewById(R.id.btn_tambah_data);
        btnLihatData = findViewById(R.id.btn_lihat_data);
        btnKeluar = findViewById(R.id.btn_keluar);
        btnTambahData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tambahDataIntent = new Intent(MainActivity.this, TambahDataActivity.class);
                startActivity(tambahDataIntent);
            }
        });
        btnLihatData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lihatdataIntent = new Intent(MainActivity.this, LihatDataActivity.class);
                startActivity(lihatdataIntent);
            }
        });
        btnKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
}
