package com.reksa.karang.greendeahansnotes;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.reksa.karang.greendeahansnotes.adapter.LihatDataAdapter;
import com.reksa.karang.greendeahansnotes.model.Data;

import java.util.ArrayList;
import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class LihatDataActivity extends AppCompatActivity {

    LihatDataAdapter mAdapter;
    List<Data> listData;
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_data);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        String nama = getIntent().getStringExtra("nama");
        String tanggal = getIntent().getStringExtra("tanggal");
        String stok = getIntent().getStringExtra("stok");

        Data data = new Data(nama, tanggal, stok);
        listData = new ArrayList<>();
        listData.add(data);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new LihatDataAdapter(listData, this);
        mRecyclerView.setAdapter(mAdapter);

    }
}



















