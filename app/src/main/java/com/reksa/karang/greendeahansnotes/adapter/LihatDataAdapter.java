package com.reksa.karang.greendeahansnotes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reksa.karang.greendeahansnotes.R;
import com.reksa.karang.greendeahansnotes.model.Data;

import java.util.List;

public class LihatDataAdapter extends RecyclerView.Adapter<LihatDataAdapter.MyViewHolder> {

    private List<Data> dataList;
    private Context mContext;

    public LihatDataAdapter(List<Data> dataList, Context context) {
        this.dataList = dataList;
        mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lihat_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txtNamaBarang.setText(dataList.get(position).getNamaBarang());
        holder.txtTanggalMasuk.setText(dataList.get(position).getTanggalBarang());
        holder.txtSisaStok.setText(dataList.get(position).getSisaStok());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtNamaBarang, txtTanggalMasuk, txtSisaStok;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtNamaBarang = itemView.findViewById(R.id.txt_nama_barang);
            txtTanggalMasuk = itemView.findViewById(R.id.txt_tanggal_masuk);
            txtSisaStok = itemView.findViewById(R.id.txt_sisa_stok);
        }
    }

}
