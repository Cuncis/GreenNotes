package com.reksa.karang.greendeahansnotes.model;

/**
 * Created by Karang R. Ginolla on 14-Jun-18.
 */

public class Data {
    private String id, namaBarang, tanggalBarang, sisaStok;

    public Data() {
    }

    public Data(String namaBarang, String tanggalBarang, String sisaStok) {
        this.namaBarang = namaBarang;
        this.tanggalBarang = tanggalBarang;
        this.sisaStok = sisaStok;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getTanggalBarang() {
        return tanggalBarang;
    }

    public void setTanggalBarang(String tanggalBarang) {
        this.tanggalBarang = tanggalBarang;
    }

    public String getSisaStok() {
        return sisaStok;
    }

    public void setSisaStok(String sisaStok) {
        this.sisaStok = sisaStok;
    }
}
